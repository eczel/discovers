//
//  sendPing.swift
//  discover
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

class sendPing : UIViewController
{
    @IBOutlet weak var shareImage: UIImageView!
    @IBOutlet weak var caption: UITextView!
    @IBOutlet weak var shareLocation: UITextView!
    @IBOutlet weak var sendBtn: UIButton!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        sendBtn.layer.cornerRadius = 14
        sendBtn.clipsToBounds = true
        
        caption.layer.cornerRadius = 18
        caption.layer.shadowOpacity = 0.2
        caption.layer.shadowOffset = CGSize(width: -3, height: -3)
        caption.clipsToBounds = false
        
        shareLocation.layer.cornerRadius = 18
        shareLocation.layer.shadowOpacity = 0.2
        shareLocation.layer.shadowOffset = CGSize(width: -3, height: -3)
        shareLocation.clipsToBounds = false
        
    }
    @IBAction func tapImage(_ sender: Any) {
    }
    
    @IBAction func sendBtnTapped(_ sender: Any)
    {
        
    }
}
