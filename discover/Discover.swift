//
//  ViewController.swift
//  discover
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit

class Discover : UIViewController
{
    @IBOutlet weak var tableDiscover: UITableView!
    
    var discoverImageData: [UIImage] = [UIImage(named: "priscilla-du-preez-697322-unsplash")!]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        tableDiscover.delegate = self as UITableViewDelegate
        tableDiscover.dataSource = self
        addDiscover()
    }
    
    func addDiscover()
    {
        let newImage = UIImage(named: "priscilla-du-preez-697322-unsplash")
        discoverImageData.append(newImage!)
        
        let newImage2 = UIImage(named: "cascade")
        discoverImageData.append(newImage2!)
    }
}

