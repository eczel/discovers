//
//  extension.swift
//  discover
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import UIKit

extension Discover : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return discoverImageData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "discoverCell", for: indexPath) as? discoverCell
        cell!.discoversImage.image = discoverImageData[indexPath.row]
        cell?.delegate = self
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return (UIScreen.main.bounds.height / 2) - 10
    }
}

extension Discover: discoverCellDelegate
{
    func discoverTappedComment()
    {
        let commentVC = self.storyboard?.instantiateViewController(withIdentifier: "commentViewController")
        self.present(commentVC!, animated: true, completion: nil)
        
        
        
        //        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        //
        //        guard let navigateVC = mainStoryboard.instantiateViewController(withIdentifier: "commentViewController") as? UIViewController
        //            else{return}
        //
        //        present(navigateVC, animated: true, completion: nil)
    }
}

