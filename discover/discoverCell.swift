//
//  discoverCell.swift
//  discover
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import UIKit

protocol discoverCellDelegate
{
    //func discoverTappedClap(_sender: discoverCell)
    func discoverTappedComment()
    
}

class discoverCell: UITableViewCell
{
    @IBOutlet weak var profilesImage: UIImageView!
    @IBOutlet weak var profilesName: UILabel!
    @IBOutlet weak var smallping: UIImageView!
    @IBOutlet weak var locations: UILabel!
    @IBOutlet weak var discoversDiscription: UITextView!
    @IBOutlet weak var discoversClap: UIButton!
    @IBOutlet weak var discoversComment: UIButton!
    @IBOutlet weak var discoversPingShare: UIButton!
    @IBOutlet weak var coversView: UIView!
    @IBOutlet weak var discoversImage: UIImageView!
    @IBOutlet weak var discriptionsView: UIView!
    
    var delegate: discoverCellDelegate?
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        profilesImage.layer.cornerRadius = 33
        profilesImage.clipsToBounds = true
        
        discoversImage.layer.cornerRadius = 20
        
        discriptionsView.layer.cornerRadius = 20
        discriptionsView.alpha = 0.2
        
        discoversDiscription.isScrollEnabled = false
        
        //                          ------long tap------
        let hideViewGesture = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(press:)))
        hideViewGesture.minimumPressDuration = 1
        coversView.addGestureRecognizer(hideViewGesture)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    @objc func longPressed(press : UILongPressGestureRecognizer)
    {
        self.coversView.isHidden = true
        self.discriptionsView.isHidden = true
        
        let elapsed: Double = 0.0
        let delay = max(0.0, 3.0 - elapsed)
        DispatchQueue.main.asyncAfter(deadline: .now() + delay)
        {
            self.coversView.isHidden = false
            self.discriptionsView.isHidden = false
        }
    }
    
    //    @IBAction func clapTapped(_ sender: UIButton)
    //    {
    //        delegate?.discoverTappedClap(_sender: self)
    //    }
    
    @IBAction func commentsTapped(_ sender: UIButton)
    {
        delegate?.discoverTappedComment()
    }
}


