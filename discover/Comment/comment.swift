//
//  comment.swift
//  discover
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//

import Foundation
import  UIKit

class comment: UIViewController
{
    @IBOutlet weak var tableComment: UITableView!
    @IBOutlet weak var commentView: UIView!
    @IBOutlet weak var commentTxt: UITextField!
    @IBOutlet weak var commentBtn: UIButton!
    
    var commentData = ["Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed","Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed","Lorem ipsum dolor sit amet","Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed"]
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        tableComment.delegate = self
        tableComment.dataSource = self
        
        commentBtn.layer.cornerRadius = 25
        commentBtn.clipsToBounds = true
        tableComment.reloadData()
    }
    
    func addComment()
    {
        let newComment: String = "\(commentTxt.text ?? "")"
        commentData.append(newComment)
    }
    
    @IBAction func commentBtnTapped(_ sender: Any)
    {
        addComment()
    }
}

extension comment : UITableViewDelegate, UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return commentData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell", for: indexPath) as? commentCell
        cell!.userComment.text = commentData[indexPath.row]
        return cell!
    }
    
    //    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    //    {
    //        return
    //        return commentView.frame.height + 60
    //    }
}
