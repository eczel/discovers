//
//  commentCell.swift
//  discover
//
//  Created by Elveen on 16/04/2019.
//  Copyright © 2019 BetaMaju. All rights reserved.
//


import UIKit

class commentCell: UITableViewCell
{
    @IBOutlet weak var profileImageComment: UIImageView!
    @IBOutlet weak var clapComment: UIButton!
    @IBOutlet weak var nameTxt: UILabel!
    @IBOutlet weak var userComment: UITextView!
    
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
        // Initialization code
        
        profileImageComment.layer.cornerRadius = 78/2
        profileImageComment.clipsToBounds = true
        
        userComment.isScrollEnabled = false
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool)
    {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}

